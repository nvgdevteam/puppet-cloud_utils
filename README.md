# Cloud Utilities Puppet Module

Puppet module to install common cloud utilities.


## Prerequisites
Create a base CentOS7 image as described [here](https://github.com/hysds/hysds-framework/wiki/Puppet-Automation#create-a-base-centos-7-image-for-installation-of-all-hysds-component-instances).


## Installation
As _root_ run:
```
bash < <(curl -skL https://bitbucket.org/nvgdevteam/puppet-cloud_utils/raw/master/install.sh)
```
